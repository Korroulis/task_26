//////////  First clear all existing cookies

document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

//////////  Cookie manager class

class CookieManager {
    constructor(name) {
      this.name = name;
    } 
    // Set cookie method
    setCookie(name,...args) { 
        let time = new Date();
        time.setTime(time.getTime() + (15*60*1000));
        let expiry = "expires="+ time.toUTCString();
        document.cookie = name + "=" + args[0] + ";" + expiry + "; path=/" + args[1]+"; secure:"+`${args[2]}` +"; samesite="+args[3];
    };
    // Get method cookie
   getCookie(name) {
    console.log(document.cookie);    
    var pattern = RegExp(name + "=.[^;]*")
    var matched = document.cookie.match(pattern)
    if(matched){
        var cookie = matched[0].split('=')
        console.log(cookie[1]);
    }
    else alert('Insert correct key name');
    }; 
    // Clear cookie method
   clearCookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;  path=/; ';
    }; 
};

//////////  Insert cookies  

Cookie1 = new CookieManager("Mihalis");
Cookie1.setCookie('Mihalis','bread','',true,'lax');

Cookie1 = new CookieManager("John");
Cookie1.setCookie('John','banana','',true,'lax');

Cookie1 = new CookieManager("Covid");
Cookie1.setCookie('Covid','deadly','',true,'lax');


Cookie1.getCookie('John');

console.log(document.cookie);

///////////  Clear cookies

Cookie1.clearCookie('John');
Cookie1.clearCookie('Mihalis');

console.log(document.cookie); 

